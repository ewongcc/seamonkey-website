[% BLOCK download_build;
     # vars that need to be set by caller or document:
     # smversion: version number (2.0a1, 2.0.1, ...)
     # platform: platform (win, linux, osx)
     # locale: locale (en-US, de, ...)
     # mirrorred: bool that tells us if to use release mirrors/bouncer, false means direct FTP
     # uselongversion: bool that tells us if to use the longversion, false means no
     # usefulldetails: bool that, if true, tells us to use locale and platform

     IF mirrorred;
       "https://download.mozilla.org/?product=seamonkey-$smversion&amp;os=$platform&amp;lang=$locale";
     ELSE;
       ftpdir = "https://archive.mozilla.org/pub/seamonkey/releases/$smversion";
       IF smversion.match('^1\.') OR smversion.match('^2\.0a');
         IF platform == "win";
           "$ftpdir/seamonkey-${smversion}.${locale}.win32.installer.exe";
         ELSIF platform == "osx";
           "$ftpdir/seamonkey-${smversion}.${locale}.mac.dmg";
         ELSIF platform == "linux";
           IF smversion.match('^1\.');
             "$ftpdir/seamonkey-${smversion}.${locale}.linux-i686.installer.tar.gz";
           ELSE;
             "$ftpdir/seamonkey-${smversion}.${locale}.linux-i686.installer.tar.bz2";
           END;
         ELSE;
           "$ftpdir/";
         END;
       ELSIF usefulldetails;
         filebase = "$locale/seamonkey-${smversion}.$locale";
         IF platform == "win";
           "$ftpdir/win32/${filebase}.win32.installer.exe";
         ELSIF platform == "win64";
           "$ftpdir/win64/${filebase}.win64.installer.exe";
         ELSIF platform == "osx";
           "$ftpdir/mac/${filebase}.mac.dmg";
         ELSIF platform == "linux";
           "$ftpdir/linux-i686/${filebase}.linux-i686.tar.bz2";
         ELSIF platform == "linux64";
           "$ftpdir/linux-x86_64/${filebase}.linux-x86_64.tar.bz2";
         ELSE;
           "$ftpdir/$platform/$locale/";
         END;
       ELSIF smversion.match('^2\.49\.5');
         IF platform == "win";
           "$ftpdir/win32/$locale/seamonkey-${smversion}.installer.exe";
         ELSIF platform == "win64";
           "$ftpdir/win64/$locale/seamonkey-${smversion}.installer.exe";
         ELSIF platform == "osx";
           "$ftpdir/mac/$locale/seamonkey-${smversion}.dmg";
         ELSIF platform == "linux";
           "$ftpdir/linux-i686/$locale/seamonkey-${smversion}.tar.bz2";
         ELSIF platform == "linux64";
           "$ftpdir/linux-x86_64/$locale/seamonkey-${smversion}.tar.bz2";
         ELSE;
           "$ftpdir/$platform/$locale/";
         END;
       ELSIF uselongversion;
         longversion = smversion|replace('a','%20Alpha%20')|replace('b','%20Beta%20')|replace('rc','%20RC%20');
         IF platform == "win";
           "$ftpdir/win32/$locale/SeaMonkey%20Setup%20${longversion}.exe";
         ELSIF platform == "win64";
           "$ftpdir/win64/$locale/SeaMonkey%20Setup%20${longversion}.exe";
         ELSIF platform == "osx";
           "$ftpdir/mac/$locale/SeaMonkey%20${longversion}.dmg";
         ELSIF platform == "linux";
           "$ftpdir/linux-i686/$locale/seamonkey-${smversion}.tar.bz2";
         ELSIF platform == "linux64";
           "$ftpdir/linux-x86_64/$locale/seamonkey-${smversion}.tar.bz2";
         ELSE;
           "$ftpdir/$platform/$locale/";
         END;
       ELSE;
         IF platform == "win";
           "$ftpdir/win32/$locale/SeaMonkey%20Setup%20${smversion}.exe";
         ELSIF platform == "win64";
           "$ftpdir/win64/$locale/SeaMonkey%20Setup%20${smversion}.exe";
         ELSIF platform == "osx";
           "$ftpdir/mac/$locale/SeaMonkey%20${smversion}.dmg";
         ELSIF platform == "linux";
           "$ftpdir/linux-i686/$locale/seamonkey-${smversion}.tar.bz2";
         ELSIF platform == "linux64";
           "$ftpdir/linux-x86_64/$locale/seamonkey-${smversion}.tar.bz2";
         ELSE;
           "$ftpdir/$platform/$locale/";
         END;
       END;
     END;
   END;
%]

[% BLOCK direct_download_file;
     "https://archive.mozilla.org/pub/seamonkey/releases/$smversion/$path";
   END;
%]

[% BLOCK download_file;
     # vars that need to be set by caller or document:
     # smversion: version number (2.0a1, 2.0.1, ...)
     # mirrorred: bool that tells us if to use release mirrors/bouncer, false means direct FTP
     # path: path to add to the base URL of that version

# releases.mozilla.org disabled, see bug 871120
#     IF mirrorred AND NOT smversion.match('a|b');
#       "http://releases.mozilla.org/pub/seamonkey/releases/$smversion/$path";
#     ELSE;
       INCLUDE direct_download_file;
#     END;
   END;
%]

[% BLOCK downloads_table;
     # vars that need to be set by caller or document:
     # languages: set of languages containing information for the table
     # smversion: version number (2.0a1, 2.0.1, ...)
     "  <table class=\"downloads\" cellpadding=\"0\" cellspacing=\"0\" summary=\"$languages.summary\">\n";
     "    <thead>\n";
     "      <tr>\n";
     FOREACH header IN languages.header;
       table_header = "<th";
       IF header.colspan;
         table_header = "$table_header colspan=\"$header.colspan\"";
       END;
       IF header.class;
         table_header = "$table_header class=\"$header.class\">";
       ELSE;
         table_header = "$table_header>";
       END;
       IF header.spanclass;
         table_header = "$table_header<span class=\"$header.spanclass\">$header.name";
         IF header.second;
           table_header = "$table_header<br>$header.second</span>";
         ELSE;
           table_header = "$table_header</span>";
         END;
       ELSE;
         table_header = "$table_header$header.name";
       END;
       "        $table_header</th>\n";
     END;
     "      </tr>\n";
     "    </thead>\n";

     "    <tfoot>\n";
     "      <tr>\n";
     FOREACH footer IN languages.footer;
       IF footer.class;
         "        <td class=\"$footer.class\"></td>\n";
       END;
       IF footer.colspan;
         "        <td colspan=\"$footer.colspan\"></td>\n";
       END;
     END;
     "      </tr>\n";
     "    </tfoot>\n";
     "    <tbody>\n";

     class = "odd";
     FOREACH language IN languages.language;
       "      <tr id=\"$language.id\" class=\"$class\">\n";

       IF class == "odd";
         class = "even";
       ELSE;
         class = "odd";
       END;

       FOREACH cell IN language.cell;
         IF cell.name;
           "        <td>$cell.name</td>\n";
         END;
         IF cell.lang;
           "        <td lang=\"$cell.lang\">$cell.native</td>\n";
         END;
         IF cell.version;
           "        <td class=\"curVersion\">$smversion</td>\n";
         END;
         IF cell.locale;
           "        <td><a href=\"";
           INCLUDE download_build platform=cell.platform locale=cell.locale;
           "\" class=\"$cell.class\" data-platform=\"$cell.platform\">$cell.content</a></td>\n";
         END;
       END;

       "      </tr>\n";
     END;

     "    </tbody>\n";
     "  </table>\n";

   END;
%]

[% BLOCK downloads_fieldset;
     # vars that need to be set by caller or document:
     # dropdowns: set of dropdowns containing information for the selects

     "  <fieldset>\n";
     FOREACH dropdown IN dropdowns.dropdown;
       "    <p class=\"$dropdown.class\">\n";
       "      <label for=\"$dropdown.id\" class=\"select-label\">$dropdown.label</label>\n";
       "      <select id=\"$dropdown.id\" class=\"select-input\" aria-controls=\"$dropdown.aria\">\n";

       FOREACH option IN dropdown.option;
         "        <option value=\"$option.value\">$option.label</option>\n";
       END;

       "      </select>\n";
       "    </p>\n";
     END;

     "  </fieldset>\n";
   END;
%]

[% BLOCK downloads_langpack;
     # vars that need to be set by caller or document:
     # languages: set of languages containing information for the langpacks
     # smversion: version number (2.0a1, 2.0.1, ...)
     "<p>\n";
     FOREACH language IN languages.language;
       "  <a href=\"";
       INCLUDE download_file path="langpack/seamonkey-${smversion}.${language.id}.langpack.xpi";
       "\">";
       FOREACH cell IN language.cell;
         IF cell.name;
           "$cell.name</a>,\n";
         END;
       END;
     END;

     "</p>\n";

   END;
%]
