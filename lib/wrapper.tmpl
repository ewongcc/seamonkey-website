[%- USE MetaExtractor -%]
[%- DEFAULT sidebar = "sidebar.tmpl"-%]
[%-# Can't use DEFAULT for wrap, since DEFAULT overwrites a false value. -%]
[%- wrap = wrap.defined ? wrap : true -%]
[%- IF wrap -%]
[%- document = MetaExtractor.extract(content) -%]
[%- USE date -%]
[%- currentyear = date.format(format = '%Y') -%]
[%- lang_use = document.lang ? document.lang : 'en-US' |replace('-','_') -%]
[%-# English Strings -%]
[%- 
    strings.listsep       = ", "
    strings.listand       = ", and "

    strings.Home          = "Home"
    strings.About         = "About"
    strings.Community     = "Community"
    strings.Download      = "Download"
    strings.Contribute    = "Contribute"
    strings.Next          = "Next"
    strings.Previous      = "Previous"
    strings.ToC           = "Table of Contents"
    strings.Sitemap       = "Site Map"
    strings.Security      = "Security Updates"
    strings.Contact       = "Contact Us"
    strings.About         = "About SeaMonkey"
    strings.WrittenBy     = "Written by"
    strings.MaintainedBy  = "Maintained by"
    strings.Contributors  = "Contributing Writers"
    strings.TranslationBy = "Translation by"
    strings.LicenseIs     = "This page is licensed under the"
    strings.TrademarkPre  = "SeaMonkey and the SeaMonkey logo are "
    strings.TrademarkLink = "registered trademarks"
    strings.TrademarkMid  = " of the "
    strings.TrademarkMoFo = "Mozilla Foundation"
    strings.TrademarkPost = "."
    strings.LicenseCC     = "Portions of this content are &copy; 1998&#8211;$currentyear by individual mozilla.org contributors; content available under a Creative Commons license"
    strings.Details       = "Details"
    strings.Modified      = "Last modified"
    strings.History       = "Document History"
-%]
[%- document.doctype %]
<html [% IF document.lang %]lang="[% document.lang %]"[%END%]
      [% IF document.dir %]dir="[% document.dir %]"[%END%]>
  <head>
    <title>[% document.title %]</title>
    <link rel="stylesheet" type="text/css" href="/css/base/template.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/base/content.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/artemia/template.css" title="Artemia Nyos" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/artemia/content.css" title="Artemia Nyos" media="screen">
    <link rel="icon" href="/images/seamonkey16.png" type="image/png">
    [%- document.head %]
  </head>
  <body id="seamonkeyproject-org">
    <div id="header"><h1><a href="/" title="Go to seamonkey-project.org" accesskey="1"><img
      src="/images/template/header-logo.png" height="38" width="260" alt="SeaMonkey Project"></a></h1></div>
    <div id="breadcrumbs">
      [% PROCESS breadcrumbs %]
    </div>
    <div id="content">
      <div id="side">
        [% INCLUDE $sidebar %]
      </div>
      <div id="mainContent">
        [% IF document.links.next || document.links.prev || document.links.toc -%]
        <ul id="tNavTop">
          [% IF document.links.prev -%]
          <li><a href="[% document.links.prev.0.href %]" rel="prev">[% strings.Previous %]: [% document.links.prev.0.title %]</a>
          [%- END -%]
          [%- IF document.links.next -%]
          <li><a href="[% document.links.next.0.href %]" rel="next">[% strings.Next %]: [% document.links.next.0.title %]</a>
          [%- END -%]
          [%- IF document.links.toc -%]
          <li><a href="[% document.links.toc.0.href %]" rel="toc">[% strings.ToC %]</a>
          [%- END %]
        </ul>
        [%- END -%]

        [%- document.content -%]

        <address>
        [%- IF document.links.author %]
          [% strings.WrittenBy %] <a href="[% document.links.author.0.href %]">[% document.links.author.0.title %]</a>
          [%- SET lastIndex = document.links.author.size - 1 -%]
          [%- IF lastIndex > 0 -%]
            [%- SET almostLastIndex = lastIndex - 1 -%]
            [%- FOREACH i = [ 1 .. almostLastIndex ] -%]
              [%- strings.listsep -%]
              <a href="[% document.links.author.$i.href %]">[% document.links.author.$i.title %]</a>
              [%- SET i = i + 1 -%]
            [%- END -%]
              [%- strings.listand -%]
              <a href="[% document.links.author.$lastIndex.href %]">[% document.links.author.$lastIndex.title %]</a>
          [%- END -%]
        [%- END -%]
        [% IF document.links.maintainer %]
          <br>
          [% strings.MaintainedBy %] <a href="[% document.links.maintainer.0.href %]">[% document.links.maintainer.0.title %]</a>
          [%- SET lastIndex = document.links.maintainer.size - 1 -%]
          [%- IF lastIndex > 0 -%]
            [%- SET almostLastIndex = lastIndex - 1 -%]
            [%- FOREACH i = [ 1 .. almostLastIndex ] -%]
              [%- strings.listsep -%]
              <a href="[% document.links.maintainer.$i.href %]">[% document.links.maintainer.$i.title %]</a>
              [%- SET i = i + 1 -%]
            [%- END -%]
              [%- strings.listand -%]
              <a href="[% document.links.maintainer.$lastIndex.href %]">[% document.links.maintainer.$lastIndex.title %]</a>
          [%- END -%]
        [%- END -%]
        [%- IF document.links.contributor %]
          <br>
          [% strings.Contributors %] <a href="[% document.links.contributor.0.href %]">[% document.links.contributor.0.title %]</a>
          [%- SET lastIndex = document.links.contributor.size - 1 -%]
          [%- IF lastIndex > 0 -%]
            [%- SET almostLastIndex = lastIndex - 1 -%]
            [%- FOREACH i = [ 1 .. almostLastIndex ] -%]
              [%- strings.listsep -%]
              <a href="[% document.links.contributor.$i.href %]">[% document.links.contributor.$i.title %]</a>
              [%- SET i = i + 1 -%]
            [%- END -%]
              [%- strings.listand -%]
              <a href="[% document.links.contributor.$lastIndex.href %]">[% document.links.contributor.$lastIndex.title %]</a>
          [%- END -%]
        [%- END -%]
        [% IF document.links.translator %]
          <br>
          [% strings.TranslationBy %] <a href="[% document.links.translator.0.href %]">[% document.links.translator.0.title %]</a>
          [%- SET lastIndex = document.links.translator.size - 1 -%]
          [%- IF lastIndex > 0 -%]
            [%- SET almostLastIndex = lastIndex - 1 -%]
            [%- FOREACH i = [ 1 .. almostLastIndex ] -%]
              [%- strings.listsep -%]
              <a href="[% document.links.translator.$i.href %]">[% document.links.translator.$i.title %]</a>
              [%- SET i = i + 1 -%]
            [%- END -%]
              [%- strings.listand -%]
              <a href="[% document.links.translator.$lastIndex.href %]">[% document.links.translator.$lastIndex.title %]</a>
          [%- END -%]
        [%- END %]
        </address>

        [%- IF document.links.next || document.links.prev || document.links.toc -%]
        <ul id="tNavBottom">
          [% IF document.links.prev -%]
          <li><a href="[% document.links.prev.0.href %]" rel="prev">[% strings.previous %]: [% document.links.prev.0.title %]</a>
          [%- END -%]
          [%- IF document.links.next -%]
          <li><a href="[% document.links.next.0.href %]" rel="next">[% strings.next %]: [% document.links.next.0.title %]</a>
          [%- END -%]
          [%- IF document.links.toc -%]
          <li><a href="[% document.links.toc.0.href %]" rel="toc">[% strings.toc %]: [% document.links.toc.0.title %]</a>
          [%- END -%]
        </ul>
        [%- END %]
      </div>
    </div>

    <div id="footer">
      <ul id="footer-menu">
        <li><a href="[% toplink %]sitemap">[% strings.Sitemap %]</a></li>
        <li><a href="http://www.mozilla.org/security/">[% strings.Security %]</a></li>
        <li><a href="[% toplink %]about">[% strings.About %]</a></li>
        <li><a href="[% toplink %]about#contact">[% strings.Contact %]</a></li>
      </ul>
      <p class="tLicense">[% strings.TrademarkPre %]<a href="[% toplink %]legal/trademark">[% strings.TrademarkLink %]</a>[% strings.TrademarkMid %]<a href="http://www.mozilla.org/foundation/">[% strings.TrademarkMoFo %]</a>[% strings.TrademarkPost %]</p>
      <p class="tLicense">[% strings.LicenseCC %] | <a href="http://www.mozilla.org/foundation/licensing/website-content.html">[% strings.Details %]</a>.</p>
      [% IF document.links.license -%]
      <p class="tLicense">[% strings.LicenseIs %]
        <a href="[% document.links.license.0.href %]">[% document.links.license.0.title %]</a>.</p>
      [%- END %]
      [%- sourcefile = template.name;
        sourcefile = sourcefile|replace('\+','%2b');
        sourcefile = sourcefile|replace('\=','%3d');
        sourcefile = sourcefile|replace('\&','%26'); -%]
      <ul class="site-tools">
        <li>[% strings.Modified %] [% date.format(template.modtime, '%B %d, %Y', lang_use) %]</li>
        <li><a href="https://hg.mozilla.org/SeaMonkey/seamonkey-project-org/log/default/src/[% sourcefile %]">[% strings.History %]</a></li>
      </ul>
    </div>

  </body>
</html>
[%- ELSE -%]
[% content %]
[%- END -%]
